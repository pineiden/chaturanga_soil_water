#include "piece.hpp"

class Square{
private:
  bool color;
  Piece piece;
  int strength;
public:
  Square(int c=0)
    :color{c}, piece{null_ptr}, strength{0} {}
};
