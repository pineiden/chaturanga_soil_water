
<header>
# Chaturanga Soil Water  #
</header>

<main>
Introduction
=

This project has the main goal to map Chess Games and visualize with
the implementation of the Soil-Water theory.

This Software will be mainly developed with the C++ language because
provides good tools to process and control in an effective way the
actions.

The Chaturanga word came from an antique game in what Chess is based.
And the Soil-Water name came from the idea that the game's goal is
to sorround the other king to checkmate and avoid to sorround our
king with the other pieces.

Every pieces has an amount of posibilities that came from the official rules.
And every possible action or square to put the pieces give us a point. Hence
a square can have *null, defensive and  ofensive* points.

The project considers a board object with two base visualizations: the game and
the soil-water mode. The pieces, with properties: set, status {in game, of game}, 
movements, history, defense points, ofensive points.
</main>
